import React, { useState } from "react";
import { UserContext } from "../context/userContext";

export const UserProvider = ({ children }) => {
  const [userList, setUserList] = useState([]);

  const updateUserList = (userListInformation) => {
    setUserList(userListInformation);
  };

  return (
    <UserContext.Provider
      value={{
        userList: userList,
        updateUserList: updateUserList,
      }}
    >
      {children}
    </UserContext.Provider>
  );
};
