import React, { useState } from "react";
import "./App.css";
import AddUserPage from "./pages/addUserPage";
import { UserProvider } from "./provider/userProvider";

function App() {
  return (
    <UserProvider>
      <AddUserPage />
    </UserProvider>
  );
}

export default App;
