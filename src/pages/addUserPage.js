import React, { useState, useContext } from "react";
import "./addUserPage.css";
import UserItem from "../components/userItem";
import AddUserModal from "../components/addUserMoadal";
import { UserContext } from "../context/userContext";

const AddUserPage = () => {
  const { userList, updateUserList } = useContext(UserContext);

  const [modalVisible, setModalVisible] = useState(false);

  return (
    <>
      <header className="header">
        <h1 className="title">FRT Project</h1>
        <input className="searchInput" placeholder="type to filter the table" />
      </header>

      <main className="main">
        <button
          className="addBtn"
          onClick={() => {
            setModalVisible(true);
          }}
        >
          +
        </button>
        <div className="container">
          <div className="sections">
            <span className="sectionTxts">USER</span>
          </div>
          <div className="sections">
            <span className="sectionTxts">ROLE</span>
          </div>
          <div className="sections">
            <span className="sectionTxts">STATUS</span>
          </div>
          <div className="sections">
            <span className="sectionTxts">ACTIONS</span>
          </div>
        </div>

        {userList &&
          userList?.map((item) => {
            return (
              <UserItem
                user={item.name}
                role={item.status}
                status={item.status}
                actions={item.actions}
              />
            );
          })}
        <button
          onClick={() => {
            updateUserList("");
          }}
        >
          Delete All
        </button>
      </main>
      <AddUserModal
        modalVisible={modalVisible}
        closeModal={() => {
          setModalVisible(false);
        }}
      />
    </>
  );
};

export default AddUserPage;
