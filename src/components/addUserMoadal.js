import React, { useState, useContext } from "react";
import Modal from "react-modal";
import "./addUserModal.css";
import { UserContext } from "../context/userContext";

const AddUserModal = ({ modalVisible, closeModal }) => {
  const { userList, updateUserList } = useContext(UserContext);

  const [name, setName] = useState();
  const [lastname, setLastName] = useState();
  const [email, setEmail] = useState();
  const [status, setStatus] = useState("Admin");

  const sendUserInfo = () => {
    if (name == "" || lastname == "" || email == "") {
      alert("fill all fields");
    } else {
      updateUserList([...userList, { name, lastname, email, status }]);
      setName("");
      setLastName("");
      setEmail("");
    }
  };

  return (
    <Modal
      isOpen={modalVisible}
      //   onAfterOpen={afterOpenModal}
      //   onRequestClose={closeModal}
      //   style={customStyles}
      contentLabel="Example Modal"
      className="modal"
    >
      <button className="closeBtn" onClick={closeModal}>
        x
      </button>
      <input
        className="input"
        placeholder="Firstname"
        value={name}
        onChange={(text) => {
          setName(text.target.value);
        }}
      />
      <input
        className="input"
        placeholder="Lastname"
        value={lastname}
        onChange={(text) => {
          setLastName(text.target.value);
        }}
      />

      <input
        className="input"
        placeholder="Email"
        value={email}
        onChange={(text) => {
          setEmail(text.target.value);
        }}
      />
      <select
        value={status}
        onChange={(event) => setStatus(event.target.value)}
        className="input"
      >
        <option value="Admin">Admin</option>
        <option value="User">User</option>
      </select>

      <button className="sendBtn" onClick={sendUserInfo}>
        Send Invitation
      </button>
    </Modal>
  );
};

export default AddUserModal;
