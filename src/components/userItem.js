import React, { useState } from "react";
import "./userItem.css";

const UserItem = ({ user, role, status, actions }) => {
  return (
    <div className="container">
      <div className="sections">
        <span className="sectionTxts">{user}</span>
      </div>
      <div className="sections">
        <span className="sectionTxts">{role}</span>
      </div>
      <div className="sections">
        <label class="switch">
          <input type="checkbox" />
          <span class="slider round"></span>
        </label>
      </div>
      <div className="sections">
        <span className="sectionTxts">{actions}</span>
      </div>
    </div>
  );
};

export default UserItem;
